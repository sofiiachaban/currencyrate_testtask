from django.shortcuts import render
from django.http import HttpResponse
from currencyrate.pbdata import *
from django.http import JsonResponse
from .forms import DateForm
from .models import *


year_rate = year_data
month_rate = month_data
week_rate = week_data


def change_rate(date,rate,daysnum):
    date = str(date).split('-')
    rate = float(rate)
    if daysnum == 7:
        for i in range(daysnum+1):
            if  week_rate[i][0]['exchangedate'] == date[2]+'.'+date[1]+'.'+date[0]:
                week_rate[i][0]['rate'] = rate
    if daysnum == 30:
        for i in range(daysnum+1):
            if  month_rate[i][0]['exchangedate'] == date[2]+'.'+date[1]+'.'+date[0]:
                month_rate[i][0]['rate'] = rate
    if daysnum == 365:
        for i in range(daysnum+1):
            if  year_rate[i][0]['exchangedate'] == date[2]+'.'+date[1]+'.'+date[0]:
                year_rate[i][0]['rate'] = rate




def week(request):
    if request.method == 'POST':
        form = DateForm(request.POST)
        if form.is_valid():
            date = form.cleaned_data["date"]
            rate = form.cleaned_data["rate"]
            change_rate(date,rate,7)
    else:
        form = DateForm()
    return render(request,'line_chart.html',{'form': form})

def month(request):
    if request.method == 'POST':
        form = DateForm(request.POST)
        if form.is_valid():
            date = form.cleaned_data["date"]
            rate = form.cleaned_data["rate"]
            change_rate(date,rate,30)
    else:
        form = DateForm()
    return render(request,'line_chart_month.html',{'form': form})

def year(request):
    if request.method == 'POST':
        form = DateForm(request.POST)
        if form.is_valid():
            date = form.cleaned_data["date"]
            rate = form.cleaned_data["rate"]
            change_rate(date,rate,365)
    else:
        form = DateForm()
    return render(request,'line_chart_year.html',{'form': form})


def line_chart(request,num):
    if num == 7:
        data = load_exchange(num,week_rate)
        labels = load_labels(num,week_rate)
    elif num == 30:
        data = load_exchange(num,month_rate)
        labels = load_labels(num,month_rate)
    elif num == 365:
        data = load_exchange(num,year_rate)
        labels = load_labels(num,year_rate)

    return JsonResponse(data={
        'labels': labels,
        'data': data,
    })




    