import re
import requests
import json
import datetime
from .models import *

URL = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode=USD&json&date='


def load_data(daysnum):
    if daysnum == 7:
        data = week_data
    elif daysnum == 30:
        data = month_data
    elif daysnum == 365:
        data = year_data
    return data


def load_labels(daysnum,data):
    labels = []
    for i in range(daysnum+1):
        labels.append(data[i][0]['exchangedate'])
    return labels


def load_exchange(daysnum,data):
    exchange_data = []
    for i in range(daysnum+1):
        exchange_data.append(data[i][0]['rate'])
    return exchange_data


