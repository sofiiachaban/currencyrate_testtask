from django.apps import AppConfig


class CurrencyrateConfig(AppConfig):
    name = 'currencyrate'
