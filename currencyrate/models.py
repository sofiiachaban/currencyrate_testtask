from django.db import models
import datetime
import requests



URL = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode=USD&json&date='

def load_data(daysnum):
    today = datetime.datetime.now()
    daysagodate = today - datetime.timedelta(days=daysnum)
    data = []
    for i in range(daysnum+1):
        curr_date = daysagodate + datetime.timedelta(days=i)
        year = str(curr_date.year)
        month = '0'+ str(curr_date.month) if curr_date.month <10 else str(curr_date.month)
        day = '0'+ str(curr_date.day) if curr_date.day <10 else str(curr_date.day)
        format_date = year+month+day
        data.append(requests.get(URL+format_date).json())
    return data


year_data = load_data(365)
month_data = year_data[365-30:]
week_data = year_data[365-7:]