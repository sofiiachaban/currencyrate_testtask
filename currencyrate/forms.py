from django import forms

class DateInput(forms.DateInput):
    input_type = 'date'

class DateForm(forms.Form):
    date = forms.DateField(widget = DateInput())
    rate = forms.DecimalField(max_digits = 5, label = 'Rate')