
Exchange rate app
---
This app is used to show the exchange rate (USD to UAH). Exchange rate data is taken from the website of National Bank of Ukraine (https://bank.gov.ua/ua/open-data/api-dev). It may take up to 1 minute for app to load the data.


Setup
---

The first thing to do is to clone the repository:

```
git clone https://sofiiachaban@bitbucket.org/sofiiachaban/currencyrate_testtask.git
cd currencyrate_testtask
```
Create a virtual environment to install dependencies in and activate it:

```
python3 -m venv env
source env/bin/activate
```

Then install the dependencies:
```
pip install -r requirements.txt
```

Once ``` pip ``` has finished downloading the dependencies:
And navigate to ```http://127.0.0.1:8000/currencyrate/week/ ```

 Here you'll see exchange rate for the last week.

More
---
By clicking the buttons 'year', 'week', 'month' you'll get the chart of appropriate period.
Also you can change exchange rate by selecting the date and entering the value of exchange rate by yourself. The changes will be shown on chart.
